terraform {
  backend "s3" {
    bucket = "YOUR_BUCKET"
    key    = "pritunl.tfstate"
    region = "us-east-1"
  }
}
